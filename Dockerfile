FROM ghcr.io/graalvm/graalvm-ce:21.0.0.2 as builder
WORKDIR /opt/graalvm
RUN gu install native-image \
  && curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > lein \
  && chmod +x lein && ./lein upgrade
COPY project.clj .
RUN ./lein deps
COPY . .
RUN ./lein native-image

RUN chmod +x /opt/graalvm/target/default+uberjar/memorize
ENTRYPOINT ["/opt/graalvm/target/default+uberjar/memorize"]
CMD ["--help"]
