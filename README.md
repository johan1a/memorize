# memorize

Basic CLI flashcards program.

## Installation

Requires leiningen. (https://leiningen.org/)

# Usage with leiningen

```
lein run decks list

# See below for more examples.
# Just change 'memorize' to 'lein run'
```

# Installation (GraalVM native image)

Greatly reduces startup time.

```
paru -S jdk11-graalvm-bin
sudo $GRAALVM_HOME/bin/gu install native-image
lein native-image

# Put it somewhere on your path
# e.g.
cp ./target/default+uberjar/memorize ~/.local/bin/
```

## Usage (native image)

```
memorize # args

# Create a deck
memorize decks create --name tyska \
  --fields tyska,svenska \

# Import cards
memorize import --deck tyska --from-file words_tyska.csv

memorize decks create --name kinesiska \
  --fields hanzi,pinyin,svenska \

memorize import --deck kinesiska --from-file words_kinesiska.csv

# List all decks
memorize decks list

# See stats, number of due cards, etc.
memorize stats

# Show all cards in deck
memorize decks show kinesiska

# Start studying
memorize study tyska

# To edit a card, write /edit instead of the answer while studying.

# Delete a card with /delete
```

## Options

```
Add config to ~/.config/memorize/memorize.yaml

decks:
  kinesiska:
    strategy: self-assess
    answer-field: hanzi # Only show field "hanzi" in the answer, never in the question
    answer-info-field: pinyin # Show this alongside the answer
    due-cards-per-session: 10
    new-cards-per-session: 3
    new-card-threshold: 30 # Don't add new cards if there are more due cards than this
  kinesiska-meningar:
    strategy: self-assess
    answer-field: hanzi
    answer-info-field: pinyin
    due-cards-per-session: 10
    new-cards-per-session: 2
    new-card-threshold: 6
  tyska:
    strategy: default
    answer-field: tyska
    due-cards-per-session: 10
    new-cards-per-session: 3
    new-card-threshold: 200
```

# Development

## Running tests

```
lein test
```

## Creating a line coverage report

```
lein cloverage
```


## License

Copyright © 2021 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
