#!/usr/bin/fish

for character in (cat "characters.txt")
  translate -f hanzi $character >> output.csv
  sleep 1
end

echo hanzi,pinyin,svenska > output_escaped.csv

for line in (cat output.csv)
  set hanzi (echo $line | awk '{print $1}')
  set pinyin (echo $line | awk '{print $2}')
  # ​ replace didn't work. I did it manually in nvim afterwards
  set english (echo $line | awk '{$1=$2=""; print $0}' | sed -e 's/^[ \t]*//' | sed 's/\"/\\\'/g' | sed 's/​//g' )
  echo $hanzi,$pinyin,\"$english\" >> output_escaped.csv
end
