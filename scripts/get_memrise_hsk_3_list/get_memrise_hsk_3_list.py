#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
import os.path
import time


def download_html(cache_dir):
    for i in range(2, 97, 2):
        print(i)
        url = "https://app.memrise.com/course/983/hsk-level-3/{}/".format(i)
        response = requests.get(url)
        html = response.text
        cached_file = '{}/{}.html'.format(cache_dir, i)
        with open(cached_file, "w") as file:
            file.write(html)
        print("saved " + str(i))
        time.sleep(2)


def convert_html(cache_dir):
    output_file = "characters.txt"
    for i in range(12, 97, 2):
        html_file = '{}/{}.html'.format(cache_dir, i)
        with open(html_file, "r") as file:
            html = file.read()
        soup = BeautifulSoup(html, 'html.parser')
        hanzi_columns = soup.find_all(class_="col_b")
        hanzi = [col.text + "\n" for col in hanzi_columns]

        with open(output_file, "a") as file:
            file.writelines(hanzi)
        print(hanzi)


if __name__ == "__main__":
    cache_dir = 'cache/hsk-level-3'
    os.makedirs(cache_dir, exist_ok=True)

    convert_html(cache_dir)
