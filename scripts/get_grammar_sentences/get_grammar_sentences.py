#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
import os.path
import time
import base64


def download_url(cache_dir, url):
    os.makedirs(cache_dir, exist_ok=True)

    message_bytes = url.encode('utf-8')
    base64_bytes = base64.b64encode(message_bytes)
    base64_url = base64_bytes.decode('utf-8')

    cached_file = '{}/{}.html'.format(cache_dir, base64_url)

    if os.path.isfile(cached_file):
        with open(cached_file) as file:
            html = file.read()
    else:
        response = requests.get(url)
        html = response.text
        with open(cached_file, "w") as file:
            file.write(html)
        time.sleep(1)
    return html


def parse_article(html):
    soup = BeautifulSoup(html, 'html.parser')
    examples = soup.select(".liju li")
    for example in examples:
        marked_x = False
        try:
            if "x" in example['class']:
                marked_x = True
        except Exception:
            pass

        if not marked_x:
            pinyin_elem = example.find(class_='pinyin')
            translation_elem = example.find(class_='trans')
            if pinyin_elem is not None and translation_elem is not None:
                pinyin = pinyin_elem.text.strip().replace('"', '\'')
                translation = translation_elem.text.strip().replace('"', '\'')

                dot = """。"""
                question_mark = """？"""
                exclamation_point = """！"""
                ending = ""
                if dot in example.text:
                    splitted = example.text.split(dot)
                    ending = dot
                elif question_mark in example.text:
                    splitted = example.text.split(question_mark)
                    ending = question_mark
                else:
                    splitted = example.text.split(exclamation_point)
                    ending = exclamation_point

                hanzi = splitted[0].replace(" ", "").strip().replace('"', '\'') + ending
                line = '"{}","{}","{}"'.format(hanzi, pinyin, translation)
                print(line)


def download_html(cache_dir):
    base_urls = [
        'https://resources.allsetlearning.com/chinese/grammar/HSK_1_grammar_points',
        'https://resources.allsetlearning.com/chinese/grammar/HSK_2_grammar_points',
        'https://resources.allsetlearning.com/chinese/grammar/HSK_3_grammar_points',
    ]

    for url in base_urls:
        html = download_url(cache_dir, url)
        soup = BeautifulSoup(html, 'html.parser')
        article_hrefs = [x['href'] for x in soup.find_all(class_="mw-redirect")]

        article_base_url = "https://resources.allsetlearning.com"
        for article_href in article_hrefs:
            article_url = article_base_url + article_href
            article_html = download_url(cache_dir, article_url)
            parse_article(article_html)


if __name__ == "__main__":
    cache_dir = 'cache'
    os.makedirs(cache_dir, exist_ok=True)
    print("hanzi,pinyin,svenska")
    download_html(cache_dir)
