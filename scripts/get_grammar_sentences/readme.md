
# Installation

```
# Requires pipenv
pipenv install -r requirements.txt
```

# Running
```
pipenv run python get_grammar_sentences.py > sentences.csv
```
