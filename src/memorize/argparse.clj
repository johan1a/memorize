(set! *warn-on-reflection* true)
(ns memorize.argparse
  (:require [memorize.utils :refer [index-of]])
  (:gen-class))

(defn get-arg-val
  [args arg-name]
  (let [index (index-of args arg-name)
        value-index (+ index 1)]
    (if (or (= index -1) (>= value-index (count args)))
      nil
      (nth args value-index))))

(defn get-arg-val-or-first
  [args arg-name]
  (let [value (get-arg-val args arg-name)]
    (or value (first args))))
