(set! *warn-on-reflection* true)
(ns memorize.strategy.common
  (:require [memorize.date :as date]
            [memorize.cards :as cards]
            [clojure.edn :as edn]
            [clojure.string :as string])
  (:gen-class))

(def wait-time-by-score
  {0 (java.time.Duration/ofHours 0)
   1 (java.time.Duration/ofHours 1)
   2 (java.time.Duration/ofHours 3)
   3 (java.time.Duration/ofHours 8)
   4 (java.time.Duration/ofDays 1)
   5 (java.time.Duration/ofDays 3)
   6 (java.time.Duration/ofDays 7)
   7 (java.time.Duration/ofDays 14)
   8 (java.time.Duration/ofDays 31)
   9 (java.time.Duration/ofDays 62)
   10 (java.time.Duration/ofDays 124)})

(defn add-to-datetime
  [^java.time.LocalDateTime datetime ^java.time.Duration duration]
  (.plus datetime duration))

(defn update-due
  [card]
  (let [duration (get wait-time-by-score (:score card))
        new-due (add-to-datetime (date/now-utc) duration)]
    (assoc card :due (date/format-date new-due))))

(defn inc-score
  [card score]
  (update-in card [:score] #(max 0 (min 10 (+ score %)))))

(defn add-answer
  "Add an answer to the card."
  [card score]
  (let [prev-answers (:answers card)
        new-answer {:datetime (date/now-as-string) :score score}
        new-answers (conj prev-answers new-answer)]
    (assoc card :answers new-answers)))

(defn on-correct-answer
  ([card] (on-correct-answer card 1))
  ([card score]
   (let [updated-score (inc-score card score)
         updated-answers (add-answer updated-score score)]
     (cards/update-card (update-due updated-answers)))))

(defn on-incorrect-answer
  [card]
  (let [updated-score (inc-score card -1)
        updated-answers (add-answer updated-score -1)]
    (cards/update-card (update-due updated-answers))))
