(set! *warn-on-reflection* true)
(ns memorize.strategy.default
  (:require [memorize.strategy.common :as common]
            [memorize.deck :refer [answer-fields queryable-fields]]
            [memorize.cards :as cards]
            [memorize.command :as command]
            [memorize.io :as io]
            [clojure.string :as string])
  (:gen-class))

(defn- study-card
  ([field-for-query field-for-answer card]
   (study-card field-for-query field-for-answer card true))
  ([field-for-query field-for-answer card should-update-score?]
   (let [content (:content card)
         content-to-show (field-for-query content)
         correct-answer (field-for-answer content)]
     (println (str (name field-for-query) ": " content-to-show))
     (print (str (name field-for-answer) ": "))
     (println "")
     (let [answer (string/trim (io/get-input))]
       (if (command/is-command? answer)
         (do
           (command/handle-command answer card)
           (let [updated-card (cards/get-card (:id card))]
             (when updated-card
               (study-card field-for-query field-for-answer updated-card should-update-score?))))
         (if (= answer correct-answer)
           (do
             (println "correct!")
             (println "")
             (when should-update-score? (common/on-correct-answer card)))
           (do
             (println "Incorrect, the correct answer is:" correct-answer)
             (println "")
             (when should-update-score? (common/on-incorrect-answer card))
             (recur field-for-query field-for-answer card false))))))))

(defn- study-card-random-field
  [opts deck card]
  (let [queryable (queryable-fields deck opts)
        field-for-answer (rand-nth (answer-fields deck opts))
        field-for-query (rand-nth (filter #(not (= field-for-answer %)) queryable))]
    (study-card field-for-query field-for-answer card)))

(defn study-cards
  [opts deck cards]
  (dorun (map #(study-card-random-field opts deck %) cards)))

