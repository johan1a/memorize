(set! *warn-on-reflection* true)
(ns memorize.strategy.self-assess
  (:require [memorize.cards :as cards]
            [memorize.strategy.common :as common]
            [memorize.io :as io]
            [memorize.command :as command]
            [memorize.date :as date]
            [memorize.deck :refer [answer-fields queryable-fields get-deck fields-of-deck]]
            [memorize.argparse :refer [get-arg-val-or-first]]
            [clojure.string :as string])
  (:gen-class))

(defn self-assess
  []
  (println "score? [0-3]")
  (let [input (string/lower-case (string/trim (io/get-input)))]
    (case input
      "0" -1
      "1" 0
      "2" 1
      "3" 2
      (recur))))

; TODO refactor this
(defn- study-card
  ([field-for-query field-for-answer answer-info-field card]
   (let [content (:content card)
         content-to-show (field-for-query content)
         correct-answer (field-for-answer content)]
     (println "")
     (println (str (name field-for-query) ": " content-to-show))
     (println (str (name field-for-answer) "? "))
     (println "")
     (let [input (string/trim (io/get-input))]
       (if (command/is-command? input)
         (do
           (command/handle-command input card)
           (let [updated-card (cards/get-card (:id card))]
             (when updated-card
               (study-card field-for-query field-for-answer answer-info-field updated-card))))
         (do
           (println "correct answer:" correct-answer)
           (when answer-info-field
             (let [answer-info (answer-info-field content)]
               (println (str (name answer-info-field) ": " answer-info))))
           (common/on-correct-answer card (self-assess))))))))

(defn- study-card-random-field
  [config deck card]
  (let [queryable (queryable-fields deck config)
        field-for-answer (rand-nth (answer-fields deck config))
        field-for-query (rand-nth (filter #(not (= field-for-answer %)) queryable))
        answer-info-field (keyword (:answer-info-field config))]
    (study-card field-for-query field-for-answer answer-info-field card)))

(defn study-cards
  [config deck cards]
  (println "Press enter after each card to show answer.")
  (dorun (map #(study-card-random-field config deck %) cards)))
