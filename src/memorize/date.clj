(set! *warn-on-reflection* true)
(ns memorize.date
  (:require [java-time :as time])
  (:gen-class))

(def date-format "YYYY-MM-dd HH:mm:ss.SSS")

(defn format-date
  [date]
  (time/format date-format date))

(defn now-utc
  []
  (java.time.LocalDateTime/now (java.time.ZoneOffset/UTC)))

(defn now-as-string
  []
  (format-date (now-utc)))

