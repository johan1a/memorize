(set! *warn-on-reflection* true)
(ns memorize.db
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.java.io :as io]
            [memorize.config :refer [default-config]])
  (:gen-class))

(defn data-dir
  []
  (let [data-home (System/getenv "XDG_DATA_HOME")
        default (str (System/getenv "HOME") "/.local/share")]
    (or data-home default)))

(defn get-db-path
  []
  (str (data-dir) "/memorize/memorize.sqlite"))

(def db
  {:dbtype "sqlite"
   :dbname (get-db-path)})

(def create-decks
  (jdbc/create-table-ddl :decks
                         [[:id :integer :primary :key :autoincrement]
                          [:name "text" :unique]
                          [:fields "text"]]))

(def create-cards
  (jdbc/create-table-ddl :cards
                         [[:id :integer :primary :key :autoincrement]
                          [:deck_id :int]
                          [:score :int]
                          [:due :date]
                          [:content "text"]
                          [:answers "text"]]))

(def create-card-deck-id-index
  "create index cards_deck_id_ix on cards ( deck_id );")

(def add-config-to-decks
  (str "alter table decks add column config text default '" default-config "' ;"))

(def migrations-0 [create-decks
                   create-cards
                   create-card-deck-id-index])

(defn should-migrate-0?
  []
  (try
    (jdbc/query db ["select count(*) from cards"])
    false
    (catch Exception _ true)))

(def migrations-1 [add-config-to-decks])

(defn should-migrate-1?
  []
  (try
    (jdbc/query db ["select config from decks"])
    false
    (catch Exception _ true)))

(def migrations-2 [(str "alter table cards add column deleted integer default 0;")])

(defn should-migrate-2?
  []
  (try
    (jdbc/query db ["select id from cards where deleted = true limit 1;"])
    false
    (catch Exception _ true)))

(def migrations-3 [(str "alter table cards add column answers text default '[]';")])

(defn should-migrate-3?
  []
  (try
    (jdbc/query db ["select answers from cards limit 1;"])
    false
    (catch Exception _ true)))

(defn migrate
  []
  (when (should-migrate-0?)
    (io/make-parents (get-db-path))
    (jdbc/db-do-commands db migrations-0))
  (when (should-migrate-1?)
    (jdbc/db-do-commands db migrations-1))
  (when (should-migrate-2?)
    (jdbc/db-do-commands db migrations-2))
  (when (should-migrate-3?)
    (jdbc/db-do-commands db migrations-3)))
