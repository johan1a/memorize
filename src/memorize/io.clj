(set! *warn-on-reflection* true)
(ns memorize.io
  (:require [clojure.string :as string]))

(defn get-input
  "Read a line of input and remove non-printable characters"
  []
  (when-let [raw (read-line)]
    ; Sometimes backspace results in � for some reason
    (string/replace raw  "�" "")))

