(set! *warn-on-reflection* true)
(ns memorize.import
  (:require [memorize.argparse :refer [get-arg-val]]
            [memorize.cards :refer [save-cards]]
            [memorize.deck :refer [get-deck]]
            [clojure.java.io :as io]
            [clojure.data.csv :as csv])
  (:gen-class))

(defn csv-data->maps
  [csv-data]
  (map zipmap
       (->> (first csv-data)
            (map keyword)
            repeat)
       (rest csv-data)))

(defn do-import-cards
  [deck filename]
  (with-open [reader (io/reader filename)]
    (let [cards (csv-data->maps (csv/read-csv reader))
          deck-id (:id (get-deck deck))]
      (dorun (save-cards deck-id cards))
      (println "Imported cards to deck:" deck)
      (dorun (map println cards)))))

(defn import-cards
  [args]
  (let [deck (get-arg-val args "--deck")
        file (get-arg-val args "--from-file")]
    (if (or (not deck) (not file))
      (println "usage: import --deck DECK --from-file FILE.csv")
      (do-import-cards deck file))))
