(set! *warn-on-reflection* true)
(ns memorize.config
  (:require [clj-yaml.core :as yaml]
            [clojure.java.io :refer [make-parents]])
  (:gen-class))

(def default-config
  {:strategy "default",
   :new-cards-per-session 3,
   :due-cards-per-session 10,
   :new-card-threshold 50})

(defn get-config
  []
  (let [config-dir (or (System/getenv "XDG_CONFIG_HOME") (str (System/getenv "HOME") "/.config"))
        config-file (str config-dir "/memorize/memorize.yaml")]
    (make-parents config-file)
    (try
      (yaml/parse-string (slurp config-file))
      (catch java.io.FileNotFoundException _ default-config))))
