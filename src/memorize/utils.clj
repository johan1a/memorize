(set! *warn-on-reflection* true)
(ns memorize.utils
  (:gen-class))

(defn index-of
  ([ss element] (index-of ss element 0))
  ([ss element current-index]
   (let [current (first ss)]
     (if (= nil current) -1
         (if (= element current) current-index
             (recur (drop 1 ss) element (+ 1 current-index)))))))
