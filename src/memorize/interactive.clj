(set! *warn-on-reflection* true)
(ns memorize.interactive
  (:require [memorize.io :as io]
            [clojure.string :as string]
            [memorize.deck :refer [deck]]
            [memorize.stats :refer [show-stats]]
            [memorize.learn :refer [study-session]]
            [memorize.import :refer [import-cards]]))

(defn handle-input
  [args config]
      (println args)
      (case (first args)
        "learn" (study-session config (drop 1 args))
        "study" (study-session config (drop 1 args))
        "stats" (show-stats (drop 1 args))
        "deck" (deck (drop 1 args))
        "decks" (deck (drop 1 args))
        "import" (import-cards (drop 1 args))
        (println (str "unknown command: " (first args)))))

(defn interactive-mode
  [config]
  (println "Enter command: ")
  (let [input (io/get-input)]
    (if (or (= nil input) (= [] input) (= "exit" input))
      (println "Goodbye!")
      (do
        (handle-input (string/split input #"\s+") config)
        (recur config)))))
