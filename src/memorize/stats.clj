(set! *warn-on-reflection* true)
(ns memorize.stats
  (:require [memorize.argparse :refer [get-arg-val get-arg-val]]
            [memorize.cards :as cards]
            [memorize.deck :as deck]))

(defn- show-stats-for-deck
  [deck-name]
  (let [deck (deck/get-deck deck-name)
        deck-id (:id deck)
        nbr-new (count (cards/get-new-cards deck-id))
        nbr-due (count (cards/get-due-cards deck-id))
        total (count (cards/get-cards deck-id))]
    (println (str deck-name ":"))
    (println (str "new:   " nbr-new))
    (println (str "due:   " nbr-due))
    (println (str "total: " total "\n"))))

(defn show-stats
  [args]
  (let [deck-name (or (get-arg-val args "--name") (first args))]
    (if deck-name
      (show-stats-for-deck deck-name)
      (let [all-deck-names (map :name (deck/list-decks))]
        (dorun (map show-stats-for-deck all-deck-names))))))
