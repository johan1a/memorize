(set! *warn-on-reflection* true)
(ns memorize.cards
  (:require [memorize.db :refer [db]]
            [memorize.date :as date]
            [clojure.edn :as edn]
            [clojure.java.jdbc :as jdbc])
  (:gen-class))

(def never-shown -1)

(defn make-row
  [deck-id card]
  [deck-id never-shown (date/now-as-string) (str card) "[]"])

(defn prepare-for-db-insert
  [deck-id cards]
  (map #(make-row deck-id %) cards))

(defn- from-db-card
  "Map from database model to easier to use model"
  [card]
  (-> card
      (update-in [:answers] #(edn/read-string %))
      (update-in [:content] #(edn/read-string %))))

(defn- to-db-card
  "Prepare for persisting to the database"
  [card]
  (-> card
    (update-in [:answers] #(str %))
    (update-in [:content] #(str %))))

(defn get-card
  [card-id]
  (from-db-card (first (jdbc/query db ["select * from cards where id = ? and deleted = false" card-id]))))

(defn get-cards
  [deck-id]
  (map from-db-card
       (jdbc/query db ["select * from cards where deck_id = ? and deleted = false " deck-id])))

(defn get-new-cards
  [deck-id]
  (map from-db-card
       (jdbc/query db ["select * from cards where deck_id = ? and deleted = false and score = -1" deck-id])))

(defn get-due-cards
  [deck-id]
  (map from-db-card
       (jdbc/query db ["select * from cards where deck_id = ? and deleted = false and score >= 0 and due < (select datetime('now')) " deck-id])))

(defn update-card
  [card]
  (jdbc/update! db :cards (to-db-card card) ["id = ?" (:id card)]))

(defn delete-card
  [card]
  (update-card (assoc card :deleted true)))

; TODO refactor, let import create rows / prepare-for-db-insert
; let this function take cards instead
(defn save-cards
  "Saves new cards"
  [deck-id cards]
  (if-not deck-id
    (throw (ex-info (str "Deck not found for id: " deck-id) {}))
    (let [rows-to-insert (prepare-for-db-insert deck-id cards)]
      (jdbc/insert-multi! db :cards
                          [:deck_id :score :due :content :answers]
                          rows-to-insert))))

