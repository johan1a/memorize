(set! *warn-on-reflection* true)
(ns memorize.core
  (:require [memorize.interactive :refer [interactive-mode handle-input]]
            [memorize.db :refer [migrate]]
            [memorize.config :refer [get-config]])
  (:gen-class))

(defn -main
  [& args]
  (migrate)
  (let [config (get-config)]
    (case (first args)
      nil (interactive-mode config)
      (handle-input args config))))
