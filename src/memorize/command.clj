(set! *warn-on-reflection* true)
(ns memorize.command
  (:require [memorize.cards :as cards]
            [clojure.edn :as edn]
            [clojure.string :as string])
  (:gen-class))

(defn- handle-edit
  [card]
  (println "Editing card:")
  (let [content (:content card)]
    (println content)
    (println "Enter field to edit, or empty to quit:")
    (let [field-to-edit (string/trim (read-line))]
      (if (empty? field-to-edit)
        (cards/update-card card)
        (do
          (println "Enter new content for field:" field-to-edit)
          (let [new-value (string/trim (read-line))
                new-content (assoc content (keyword field-to-edit) new-value)
                new-card (assoc card :content new-content)]
            (cards/update-card new-card)
            (recur (cards/get-card (:id card)))))))))

(defn- handle-delete
  [card]
  (println "deleting")
  (cards/delete-card card))

(defn is-command?
  [command]
  (or
   (= command "/edit")
   (= command "/delete")))

(defn handle-command
  [command card]
  (when (is-command? command)
    (when (= "/edit" command)
      (handle-edit card))
    (when (= "/delete" command)
      (handle-delete card))))

