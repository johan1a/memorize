(set! *warn-on-reflection* true)
(ns memorize.deck
  (:require [memorize.db :refer [db]]
            [clojure.java.jdbc :as jdbc]
            [clojure.edn :as edn]
            [memorize.cards :as cards]
            [memorize.argparse :refer [get-arg-val get-arg-val-or-first]]
            [clojure.string :refer [split]])
  (:gen-class))

(defn fields-of-deck
  [deck]
  (let [field-strings (edn/read-string (str (:fields deck)))]
    (map keyword field-strings)))

(defn queryable-fields
  [deck config]
  (let [ignored-field (keyword (:answer-info-field config))
        fields (fields-of-deck deck)]
    (filter #(not (= ignored-field %)) fields)))

(defn answer-fields
  [deck config]
  (let [answer-field (keyword (:answer-field config))]
    (if answer-field
      [answer-field]
      (queryable-fields deck config))))

(defn get-deck
  [deck-name]
  (first (jdbc/query db ["select * from decks where name = ?" deck-name])))

(defn show-deck
  [deck-name]
  (let [deck-id (:id (get-deck deck-name))
        _ (when-not deck-id (throw (ex-info (str "Deck not found: " deck-name) {})))
        cards-in-deck (cards/get-cards deck-id)]
    (println (str "Deck " deck-name ":"))
    (dorun (map println cards-in-deck))))

(defn list-decks
  []
  (jdbc/query db ["select * from decks"]))

(defn print-decks
  []
  (println "Decks:")
  (let [decks (list-decks)]
    (dorun (map println decks))))

(defn parse-fields-arg
  [field-string]
  (split field-string #","))

(defn delete-deck
  [args]
  (let [deck-name (get-arg-val-or-first args "--name")]
    (jdbc/delete! db :decks ["name = ?" deck-name])
    (println "Deleted deck:" deck-name)))

(defn keyword-or-nil
  [string]
  (if string (keyword string) nil))

(defn create-config
  [args]
  (let [strategy (or (keyword (get-arg-val args "--strategy")) :default)
        new-cards-per-session (Integer/parseInt (or (get-arg-val args "--new-cards-per-session") "3"))
        due-cards-per-session (Integer/parseInt (or (get-arg-val args "--due-cards-per-session") "10"))
        answer-info-field (or (keyword-or-nil (get-arg-val args "--answer-info-field")) nil)]
    (when-not (or (= strategy :default) (= strategy :self-assess)) (throw (ex-info (str "--strategy must be one of [default, self-assess]") {})))
    {:strategy strategy
     :new-cards-per-session new-cards-per-session
     :due-cards-per-session due-cards-per-session
     :answer-info-field answer-info-field}))

(defn create-deck
  [args]
  (let [deck-name (or (get-arg-val args "--name") (second args))
        fields (or (parse-fields-arg (get-arg-val args "--fields")) '())
        config (create-config args)]
    (try
      (jdbc/insert! db :decks {:name deck-name :fields fields :config config})
      (catch Exception _ (ex-info (str "Deck already exists: " deck-name) {})))
    (println "Created deck:" deck-name)))

(defn deck
  [args]
  (case (first args)
    nil (print-decks)
    "list" (print-decks)
    "show" (show-deck (first (drop 1 args)))
    "create" (create-deck args)
    "delete" (delete-deck (drop 1 args))))
