(set! *warn-on-reflection* true)
(ns memorize.learn
  (:require [memorize.deck :refer [get-deck fields-of-deck]]
            [memorize.cards :as cards]
            [memorize.config :refer [default-config]]
            [memorize.strategy.default :as default]
            [memorize.strategy.self-assess :as self-assess]
            [memorize.strategy.common :as common]
            [memorize.command :as command]
            [memorize.date :as date]
            [memorize.argparse :refer [get-arg-val get-arg-val-or-first]]
            [memorize.strategy.default :as default]
            [clojure.string :as string])
  (:gen-class))

(defn- print-field-with-value
  [field-and-value]
  (println (str (name (first field-and-value)) ": " (second field-and-value))))

(defn- on-showed-answer
  [card]
  (let [updated-score (common/inc-score card 1)]
    (cards/update-card (common/update-due updated-score))))

(defn- show-new-card
  [fields card]
  (let [content (:content card)
        fields-with-values (map #(identity [% (% content)]) fields)]
    (dorun (map print-field-with-value fields-with-values))
    (println "Press enter to continue...")
    (on-showed-answer card)
    (read-line)))

(defn- default-show-new-cards
  [deck cards]
  (let [fields (fields-of-deck deck)]
    (dorun (map #(show-new-card fields %) cards))))

(defn- get-show-cards-func
  [strategy]
  (case strategy
    "default" default-show-new-cards
    "self-assess" default-show-new-cards))

(defn- get-study-cards-func
  [strategy]
  (case strategy
    "default" default/study-cards
    "self-assess" self-assess/study-cards))

(defn- study-iteration
  [deck config]
  (let [deck-id (:id deck)
        all-new-cards (cards/get-new-cards deck-id)
        new-cards-per-session (:new-cards-per-session config)
        due-cards-per-session (:due-cards-per-session config)
        new-card-threshold (:new-card-threshold config)
        nbr-due-cards (count (cards/get-due-cards deck-id))
        nbr-new-cards (if (> nbr-due-cards new-card-threshold) 0 new-cards-per-session)
        new-cards (take nbr-new-cards all-new-cards)
        strategy (:strategy config)
        show-cards-func (get-show-cards-func strategy)
        study-cards-func (get-study-cards-func strategy)]
    (when-not (empty? new-cards)
      (println (str "Showing " (count new-cards) " / " (count all-new-cards) " new cards"))
      (show-cards-func deck new-cards)
      (println (str "Reviewing " (min new-cards-per-session (count all-new-cards)) " new cards"))
      (study-cards-func config deck new-cards))
    (let [all-due-cards (cards/get-due-cards deck-id)
          due-cards (take due-cards-per-session (shuffle all-due-cards))]
      (if-not (empty? due-cards)
        (do
          (println (str "Reviewing " (min due-cards-per-session (count all-due-cards)) " / " (count all-due-cards) " due cards."))
          (study-cards-func config deck due-cards)
          (println "Session complete."))
        (println "No cards are due right now")))))

(defn study-session
  [config args]
  (let [deck-name (get-arg-val-or-first args "deck")
        deck (get-deck deck-name)
        deck-config (or ((keyword deck-name) (:decks config)) default-config)]
    (when-not deck (throw (ex-info "Deck not found" {})))
    (dorun (study-iteration deck deck-config))))
