(defproject memorize "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/java.jdbc "0.7.12"]
                 [org.xerial/sqlite-jdbc "3.27.2"]
                 [org.clojure/data.csv "1.0.0"]
                 [clj-commons/clj-yaml "0.7.106"]
                 [clojure.java-time "0.3.2"]
                 [ericdallo/sqlite-jni-graal-fix "0.0.2-graalvm-21.0.0"]]
  :plugins [[io.taylorwood/lein-native-image "0.3.1"]
            [lein-cljfmt "0.7.0"]
            [lein-cloverage "1.2.2"]]
  :main memorize.core
  :target-path "target/%s"
  :native-image {:name "memorize"
                 :graal-bin :env/GRAALVM_HOME
                 :dependencies [[ericdallo/sqlite-jni-graal-fix "0.0.2-graalvm-21.0.0"]]
                 :opts ["--verbose"
                        "--initialize-at-build-time"
                        "-H:IncludeResources=db/.*|static/.*|templates/.*|.*.yml|.*.xml|.*/org/sqlite/.*|org/sqlite/.*"
                        "--no-fallback"
                        "-Dfile.encoding=UTF-8"
                        "--no-server"
                        "--allow-incomplete-classpath"
                        "--initialize-at-build-time=org.sqlite.JDBC"
                        "--initialize-at-build-time=org.sqlite.core.DB$ProgressObserver"
                        "--initialize-at-build-time=org.sqlite.core.DB"
                        "--initialize-at-build-time=org.sqlite.core.NativeDB"
                        "--initialize-at-build-time=org.sqlite.ProgressHandler"
                        "--initialize-at-build-time=org.sqlite.Function"
                        "--initialize-at-build-time=org.sqlite.Function$Aggregate"
                        "--initialize-at-build-time=org.sqlite.Function$Window"]}
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
