(ns memorize.test-utils
  (:require [clojure.test :refer [deftest testing is]]
            [memorize.utils :refer [index-of]]))

(deftest test-utils
  (testing "index-of"
    (let [arr [1 2 3]]
      (is (= 2 (index-of arr 3))))))
