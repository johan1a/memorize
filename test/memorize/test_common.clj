(ns memorize.test-common
  (:require [clojure.test :refer [deftest testing is]]
            [memorize.strategy.common :refer [on-correct-answer on-incorrect-answer]]
            [memorize.cards :refer [update-card]]))

(deftest test-common

  (testing "on-correct-answer should add an answer to the card's :answers list"
    (with-redefs [update-card (fn [card]
                                (let [answers (:answers card)
                                      answer (first answers)]
                                  (is (= 1 (count answers)))
                                  (is (= 1 (:score answer)))
                                  (is (= (count "2021-06-16 15:00:50.956") (count (:datetime answer))))))]
      (let [card {:score 0 :answers []}]
        (on-correct-answer card))))

  (testing "on-correct-answer with a given score should add an answer to the card's :answers list"
    (with-redefs [update-card (fn [card]
                                (let [answers (:answers card)
                                      answer (first answers)]
                                  (is (= 1 (count answers)))
                                  (is (= 123 (:score answer)))
                                  (is (= (count "2021-06-16 15:00:50.956") (count (:datetime answer))))))]
      (let [card {:score 0 :answers []}]
        (on-correct-answer card 123))))

  (testing "on-incorrect-answer should add an answer to the card's :answers list"
    (with-redefs [update-card (fn [card]
                                (let [answers (:answers card)
                                      answer (first answers)]
                                  (is (= 1 (count answers)))
                                  (is (= -1 (:score answer)))
                                  (is (= (count "2021-06-16 15:00:50.956") (count (:datetime answer))))))]
      (let [card {:score 0 :answers []}]
        (on-incorrect-answer card)))))
