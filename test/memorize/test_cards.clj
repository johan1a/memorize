(ns memorize.test-cards
  (:require [clojure.test :refer [deftest testing is]]
            [clojure.java.jdbc :refer [query update!]]
            [memorize.cards :refer [get-card get-cards get-new-cards get-due-cards update-card]]))

(def test-card
  {:id 189
   :score 2
   :content "{:tyska \"das Haus\", :svenska \"huset\"}"
   :answers "[{:datetime \"2021-06-16 14:34:30.874\", :score -1} {:datetime \"2021-06-17 13:34:01.181\", :score 1}]"})

(def test-card-parsed
  {:id 189
   :score 2
   :content {:tyska "das Haus", :svenska "huset"}
   :answers [{:datetime "2021-06-16 14:34:30.874", :score -1} {:datetime "2021-06-17 13:34:01.181", :score 1}]})

(deftest test-cards

  (testing "get-card should map card from db model"
    (with-redefs [query (fn [_ _]
                          [test-card])]
      (is (= (get-card 891) test-card-parsed))))

  (testing "get-cards should map card from db model"
    (with-redefs [query (fn [_ _]
                          [test-card])]
      (is (= (get-cards 654) [test-card-parsed]))))

  (testing "get-new-cards should map cards from db model"
    (with-redefs [query (fn [_ _]
                          [test-card])]
      (is (= (get-new-cards 654) [test-card-parsed]))))

  (testing "get-due-cards should map cards from db model"
    (with-redefs [query (fn [_ _]
                          [test-card])]
      (is (= (get-due-cards 654) [test-card-parsed]))))

  (testing "update-card should map card to db model"
    (with-redefs [update! (fn [_ _ card _]
                          (is (= card test-card)))]
      (update-card test-card-parsed))))
