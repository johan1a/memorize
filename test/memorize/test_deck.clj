(ns memorize.test-deck
  (:require [clojure.test :refer [deftest testing is]]
            [memorize.deck :refer [fields-of-deck queryable-fields answer-fields create-config]]))

(deftest test-deck
  (testing "fields-of-deck"
    (let [deck {:fields "[\"tyska\" \"svenska\" \"xxx\"]"}
          actual (fields-of-deck deck)]
      (is (= [:tyska :svenska :xxx] actual))))

  (testing "queryable-fields"
    (let [deck {:fields "[\"hanzi\" \"svenska\" \"pinyin\"]"}
          config {:answer-info-field "pinyin"}
          actual (queryable-fields deck config)]
      (is (= [:hanzi :svenska] actual))))

  (testing "answer-fields"
    (let [deck {:fields "[\"hanzi\" \"svenska\" \"pinyin\"]"}
          config {:answer-field "hanzi"}
          actual (answer-fields deck config)]
      (is (= [:hanzi] actual))))

  (testing "answer-fields - empty config"
    (let [deck {:fields "[\"hanzi\" \"svenska\" \"pinyin\"]"}
          config {}
          actual (answer-fields deck config)]
      (is (= [:hanzi :svenska :pinyin] actual))))

  (testing "create-config"
    (let [actual (create-config "")
          expected {:strategy :default, :new-cards-per-session 3, :due-cards-per-session 10, :answer-info-field nil}]
      (is (= expected actual)))))
