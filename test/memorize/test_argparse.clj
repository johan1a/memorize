(ns memorize.test-argparse
  (:require [clojure.test :refer [deftest testing is]]
            [memorize.argparse :refer [get-arg-val get-arg-val-or-first]]))

(deftest test-argparse
  (testing "get-arg-val"
    (let [args ["import" "--name" "tyska" "--from-file" "file.csv"]]
      (is (= "file.csv" (get-arg-val args "--from-file")))))

  (testing "get-arg-val-or-first 1"
    (let [args ["--name" "tyska" "otherstuff"]]
      (is (= "tyska" (get-arg-val-or-first args "--name")))))

  (testing "get-arg-val-or-first 2"
    (let [args ["tyska" "--from-file" "x"]]
      (is (= "tyska" (get-arg-val-or-first args "--name"))))))
